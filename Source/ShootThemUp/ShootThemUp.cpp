// Shoot Them Up Game. All rights reserved. Author: Evgenii Chernomorskii.

#include "ShootThemUp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ShootThemUp, "ShootThemUp" );

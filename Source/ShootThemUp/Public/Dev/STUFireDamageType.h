// Shoot Them Up Game. All rights reserved. Author: Evgenii Chernomorskii.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "STUFireDamageType.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTTHEMUP_API USTUFireDamageType : public UDamageType
{
	GENERATED_BODY()
	
};

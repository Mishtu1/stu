// Shoot Them Up Game. All rights reserved. Author: Evgenii Chernomorskii.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "STUIceDamageType.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTTHEMUP_API USTUIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
